from django.conf import settings
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text, force_str, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.decorators.csrf import csrf_exempt

from ToDoList import settings
from .forms import *


def tasks(request):
    taskss = Task.objects.filter(referring_user_id=request.user.username)

    form = TaskForm()
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.referring_user_id = User.objects.get(pk=request.user.username)
            obj.save()
        return redirect(tasks)

    context = {'tasks': taskss, 'form': form}
    return render(request, 'ToDo/list.html', context)


def update_task(request, pk):
    task = Task.objects.get(id=pk)
    form = TaskForm(instance=task)
    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect('/')
    context = {'form': form}
    return render(request, 'ToDo/update_task.html', context)


def delete_task(request, pk):
    item = Task.objects.get(id=pk)
    item.delete()
    return redirect(tasks)


@csrf_exempt
def signup(request, *args, **kwargs):
    user = request.user
    if user.is_authenticated:
        return redirect(tasks)
    context = {}
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            email = form.cleaned_data.get('email')
            user = authenticate(username=username, password=raw_password)
            user.save()
            domain = get_current_site(request)
            email_subject = 'Welcome to ToDo List.'
            message = render_to_string('ToDo/welcome.html',
                                       {'user': user, 'domain': domain})
            email_message = EmailMessage(
                email_subject,
                message,
                settings.EMAIL_HOST_USER,
                [email]
            )
            # TODO fail silently -> TRUE
            email_message.send(fail_silently=False)
            return redirect(login)
        context = {'form': form}
    return render(request, "ToDo/signup.html", context)


@csrf_exempt
def login(request, *args, **kwargs):
    context = {}
    user = request.user
    if user.is_authenticated:
        return redirect(tasks)

    if request.method == 'POST':
        form = UserAuthenticationForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user:
                auth_login(request, user)
                return redirect(tasks)
        context = {'form': form}
    return render(request, "ToDo/login.html", context)


def profile(request):
    user = request.user
    if not user.is_authenticated:
        return redirect(login)
    return render(request, "ToDo/profile.html")


def edit_profile(request):
    user = request.user
    if not user.is_authenticated:
        return redirect(login)
    form = UserUpdateForm(instance=user)
    context = {'form' : form}
    if request.method == 'POST':
        form = UserUpdateForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            return redirect(profile)

        context = {'form': form}
    return render(request, "ToDo/edit-profile.html", context)


def logout(request):
    auth_logout(request)
    return redirect(login)


def delete_user(request):
    user = request.user
    if not user.is_authenticated:
        return redirect(login)
    form = DeleteAccountForm(request.POST)
    if request.method == 'POST':
        form = DeleteAccountForm(request.POST)
        if form.is_valid():
            if authenticate(username=request.user.username, password=form.cleaned_data.get('password')):
                user.delete()
                return redirect(login)
            else:
                form.add_error(None, 'Wrong password!')
    context = {'form': form}
    return render(request, "ToDo/delete-account.html", context)