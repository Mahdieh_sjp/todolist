from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm

from .models import *


class TaskForm(forms.ModelForm):
    title = forms.CharField(max_length=100)
    text = forms.CharField(widget=forms.Textarea(attrs={'cols': '30', 'rows': '5'}))
    completed = forms.BooleanField(required=False)

    class Meta:
        model = Task
        fields = ['title', 'text', 'completed']


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(max_length=100, help_text='Required. Add a valid email address.')
    first_name = forms.CharField(max_length=50, help_text='Required.')
    last_name = forms.CharField(max_length=50, required=False)

    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name', 'password1', 'password2']

    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            user = User.objects.get(username=username)
        except Exception as e:
            return username
        raise forms.ValidationError(f"Username {username} is already in use.")

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        try:
            user = User.objects.get(email=email)
        except Exception as e:
            return email
        raise forms.ValidationError(f"Email {email} is already in use.")


class UserAuthenticationForm(forms.ModelForm):
    password = forms.CharField(label='password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'password')

    def clean(self):
        if self.is_valid():
            username = self.cleaned_data['username']
            password = self.cleaned_data['password']
            if not authenticate(username=username, password=password):
                raise forms.ValidationError('Invalid username or password.')


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField(max_length=100, help_text='Required. Add a valid email address.')
    first_name = forms.CharField(max_length=50, help_text='Required.')
    last_name = forms.CharField(max_length=50, required=False)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class DeleteAccountForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['password']