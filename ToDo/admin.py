from django.contrib import admin
from . import models



@admin.register(models.User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'full_name', 'date_joined', 'is_admin', 'display_tasks')
    list_filter = ('is_admin',)
    search_fields = ('username',)
    filter_horizontal = ()
    filter_vertical = ()
    fieldsets = ()


@admin.register(models.Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('title', 'updated', 'display_user', 'completed',)
    list_filter = ('referring_user__username', 'completed')
    search_fields = ('referring_user__username', 'title')
    readonly_fields = ()


