from django.urls import path, re_path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path('tasks/', views.tasks, name='list'),
    path('update_task/<str:pk>/', views.update_task, name='update_task'),
    path('delete_task/<str:pk>/', views.delete_task, name='delete_task'),

    path('', views.login, name='login'),
    path('login/', views.login, name='login'),
    path('signup/', views.signup, name='signup'),
    re_path('logout/', views.logout, name='logout'),
    path('profile/', views.profile, name="profile"),
    path('profile/delete-account/', views.delete_user, name="delete_user"),
    path('profile/edit/', views.edit_profile, name="edit_profile"),
    path('profile/edit/password-change/', auth_views.PasswordChangeView
         .as_view(template_name='ToDo/password-change.html'), name='password_change'),
    path('profile/edit/password-change/done/',
         auth_views.PasswordChangeDoneView
         .as_view(template_name='ToDo/password-change-done.html'), name='password_change_done'),

    path('password-reset/',
         auth_views.PasswordResetView.as_view(template_name='ToDo/password-reset.html')
         , name='password_reset'),
    path('password-reset/done/',
         auth_views.PasswordResetDoneView.as_view(template_name='ToDo/password-reset-done.html')
         , name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name="ToDo/password-reset-confirm.html"),
         name='password_reset_confirm'),
    path('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(template_name='ToDo/password-reset-complete.html'),
         name='password_reset_complete'),
]