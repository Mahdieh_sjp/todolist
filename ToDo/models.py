from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models


class UserManager(BaseUserManager):
    def creat_user(self, username, email, first_name, password, last_name=None):
        if username is None:
            raise ValueError('Users must have a username.')
        if email is None:
            raise ValueError('Users must have an email address.')
        user = self.model(
            username=username,
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, first_name, password, last_name=None):
        user = self.creat_user(
            username=username,
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
            password=password
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    username = models.CharField(max_length=20, primary_key=True)
    email = models.EmailField(max_length=100, unique=True)
    first_name = models.CharField(max_length=50, blank=False)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    date_joined = models.DateField(auto_now_add=True)
    last_login = models.DateField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    objects = UserManager()
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'first_name']

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

    def display_tasks(self):
        return ', '.join([task.title for task in Task.objects.select_related().filter(referring_user=self)[:2]])

    def full_name(self):
        if self.last_name is None:
            return self.first_name
        return ' '.join([self.first_name, self.last_name])

    display_tasks.short_description = 'Recent Tasks'
    full_name.short_description = 'Full Name'

    class Meta:
        ordering = ['date_joined']


class Task(models.Model):
    referring_user = models.ForeignKey('User', on_delete=models.CASCADE)
    title = models.CharField(max_length=250, default='untitled')
    text = models.TextField(blank=True)
    completed = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['referring_user__username']

    def __str__(self):
        return self.title

    def display_user(self):
        return self.referring_user.username

    display_user.short_description = 'User ID'

    def is_completed(self):
        return self.completed

    is_completed.short_description = 'Completed'

