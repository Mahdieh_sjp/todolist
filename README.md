# ToDoList

A simple todo list developed by django.
Data are stored in SQLITE.

## Pictures
 <p align="center">
    <img src = "pics/signup.png" alt="todo">
  </p>
  <br\>
  <p align="center">
    <img src = "pics/login.png" alt="todo">
  </p>
  <br\>
  <p align="center">
    <img src = "pics/forgot-pass.png" alt="todo">
  </p>
  <br\>
  <p align="center">
    <img src = "pics/homepage.png" alt="todo">
  </p>
<br\>
 <p align="center">
    <img src = "pics/update-task.png" alt="todo">
  </p>
  <br\>
   <p align="center">
    <img src = "pics/completed-tasks.png" alt="todo">
  </p>
  <br\> 
  <p align="center">
    <img src = "pics/profile.png" alt="todo">
  </p>
  <br\>
   <p align="center">
    <img src = "pics/profile-edit.png" alt="todo">
  </p>
  <br\>
   <p align="center">
    <img src = "pics/change-pass.png" alt="todo">
  </p>
  <br\>
   <p align="center">
    <img src = "pics/delete-acc.png" alt="todo">
  </p>
  <br\>

